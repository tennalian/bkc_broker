$(document).ready(function() {
    $('#fullpage').fullpage({
        anchors:['one','two','three','four'],
        navigation: true,
        navigationPosition: 'right',
    });

    $('.content').hover(
    	function(){
    		$(this).find('h3').toggleClass('hovered');
    	},
    	function(){
    		$(this).find('h3').toggleClass('hovered');
    	}
    );
});